$(document).ready(function(){

	//Set Step 1 to be initially highlighted
	$(".nav1").css({"background-color": "white", "color": "#36abe1"});
	//Nav bar animations
	$(".navElement").mouseover(function(){
		$(this).css({"background-color": "white", "color": "#36abe1"});
		$(this).siblings().css({"background-color": "#ebecee", "color": "#9ba0a4"});
		$(this).find(".greyIcon").css("opacity", "0");
		$(this).find(".blueIcon").css("opacity", "1");
	})
	.mouseleave(function(){
		$(this).find(".greyIcon").css("opacity", "1");
		$(this).find(".blueIcon").css("opacity", "0");
	});

   	$.getJSON('https://api.myjson.com/bins/3kh1l', function(data){
   		var stepOneArray = [];
   		var stepTwoArray = [];
   		var stepThreeArray = [];
   		var stepFourArray = [];
   		var stepFiveArray = [];
   		var stepSixArray = [];
   		var stepSevenArray = [];
   		for(var i = 0; i < data.friends.length; i++){
	   		if(data.friends[i].babyStep == 1){
	   			if(stepOneArray.indexOf(data.friends[i].firstName + data.friends[i].lastName === -1)){
	   		stepOneArray.push(data.friends[i].firstName + " " + data.friends[i].lastName);
	   			}
	   		}
	   		if(data.friends[i].babyStep == 2){
	   			if(stepTwoArray.indexOf(data.friends[i].firstName + data.friends[i].lastName === -1)){
	   		stepTwoArray.push(data.friends[i].firstName + " " + data.friends[i].lastName);
	   			}
	   		}
	   		if(data.friends[i].babyStep == 3){
	   			if(stepThreeArray.indexOf(data.friends[i].firstName + data.friends[i].lastName === -1)){
	   		stepThreeArray.push(data.friends[i].firstName + " " + data.friends[i].lastName);
	   			}
	   		}
	   		if(data.friends[i].babyStep == 4){
	   			if(stepFourArray.indexOf(data.friends[i].firstName + data.friends[i].lastName === -1)){
	   		stepFourArray.push(data.friends[i].firstName + " " + data.friends[i].lastName);
	   			}
	   		}
	   		if(data.friends[i].babyStep == 5){
	   			if(stepFiveArray.indexOf(data.friends[i].firstName + data.friends[i].lastName === -1)){
	   		stepFiveArray.push(data.friends[i].firstName + " " + data.friends[i].lastName);
	   			}
	   		}
	   		if(data.friends[i].babyStep == 6){
	   			if(stepSixArray.indexOf(data.friends[i].firstName + data.friends[i].lastName === -1)){
	   		stepSixArray.push(data.friends[i].firstName + " " + data.friends[i].lastName);
	   			}
	   		}
	   		if(data.friends[i].babyStep == 7){
	   			if(stepSevenArray.indexOf(data.friends[i].firstName + data.friends[i].lastName === -1)){
	   		stepSevenArray.push(data.friends[i].firstName + " " + data.friends[i].lastName);
	   			}
	   		}
   		}
   		stepOneArray.sort();
   		stepTwoArray.sort();
   		stepThreeArray.sort();
   		stepFourArray.sort();
   		stepFiveArray.sort();
   		stepSixArray.sort();
   		stepSevenArray.sort();

		//Default to step 1 on load
		if(stepOneArray.length === 0) {
			$(".stepText").hide();
		} else if(stepOneArray.length === 1) {
			$(".stepText").show();
			$("#names").html(stepOneArray[0] + " is ")
		} else if(stepOneArray.length === 2) {
			$(".stepText").show();
			$("#names").html("<span class='friendName'>" + stepOneArray[0] + "</span>" + " and " + "<span class='friendName'>" + stepOneArray[1] + "</span>" + " are ");
		} else {
			$(".stepText").show();
			$("#names").html(stepOneArray[0] + ", " + stepOneArray[0] + " and " + (stepOneArray.length - 2) + " others are ");
		};

		//Click handlers to change text of friends in stage
   		$(".nav1").click(function(){
   			$(".largeIcon").hide();
   			$(".moneyIcon").show();
			$("#stage").html("1");
			$("#titleSnippet").html("$1,000 Emergency Fund");
			$("#mainText1").html("An emergency fund is for those unexpected events in life that you can’t plan for: the loss of a job, an unexpected pregnancy, a faulty car transmission, and the list goes on and on. It’s not a matter of if these events will happen; it’s simply a matter of when they will happen.");
			$("#mainText2").html("This beginning emergency fund will keep life’s little Murphies from turning into new debt while you work off the old debt. If a real emergency happens, you can handle it with your emergency fund. No more borrowing. It’s time to break the cycle of debt!");
			if(stepOneArray.length === 0) {
				$(".stepText").hide();
			} else if(stepOneArray.length === 1) {
				$(".stepText").show();
				$("#names").html(stepOneArray[0] + " is ")
			} else if(stepOneArray.length === 2) {
				$(".stepText").show();
				$("#names").html("<span class='friendName'>" + stepOneArray[0] + "</span>" + " and " + "<span class='friendName'>" + stepOneArray[1] + "</span>" + " are ");
			} else {
				$(".stepText").show();
				$("#names").html(stepOneArray[0] + ", " + stepOneArray[0] + " and " + (stepOneArray.length - 2) + " others are ");
			};
		});

		$(".nav2").click(function(){
			$(".largeIcon").hide();
   			$(".chatIcon").show();
			$("#stage").html("2");
			$("#titleSnippet").html("Pay off all debt using the Debt Snowball");
			$("#mainText1").html("List your debts, excluding the house, in order. The smallest balance should be your number one priority. Don’t worry about interest rates unless two debts have similar payoffs. If that’s the case, then list the higher interest rate debt first.");
			$("#mainText2").html("The point of the debt snowball is simply this: You need some quick wins in order to stay pumped up about getting out of debt! Paying off debt is not always about math. It’s about motivation. Personal finance is 20% head knowledge and 80% behavior. When you start knocking off the easier debts, you will see results and you will stay motivated to dump your debt.")
			if(stepTwoArray.length === 0) {
				$(".stepText").hide();
			} else if(stepTwoArray.length === 1) {
				$(".stepText").show();
				$("#names").html("<span class='friendName'>" + stepTwoArray[0] + "</span>" + " is ")
			} else if(stepTwoArray.length === 2) {
				$(".stepText").show();
				$("#names").html("<span class='friendName'>" + stepTwoArray[0] + "</span>" + " and " + "<span class='friendName'>" + stepTwoArray[1] + "</span>" + " are ");
			} else {
				$(".stepText").show();
				$("#names").html("<span class='friendName'>" + stepTwoArray[0] + "</span>" + ", " + "<span class='friendName'>" + stepTwoArray[1] + "</span>" + " and " + (stepTwoArray.length - 2) + " others are ");
			};
		});

		$(".nav6").click(function(){
			$(".largeIcon").hide();
   			$(".houseIcon").show();
			$("#stage").html("6");
			$("#titleSnippet").html("Pay off your house early");
			$("#mainText1").html("Now it’s time to begin chunking all of your extra money toward the mortgage. You are getting closer to realizing the dream of a life with no house payments.");
			$("#mainText2").html("As you attack this last debt, you will gain momentum much like you did back in the second step of the debt snowball. Remember, having absolutely no payments is totally within your reach!")
			if(stepSixArray.length === 0) {
				$(".stepText").hide();
			} else if(stepSixArray.length === 1) {
				$(".stepText").show();
				$("#names").html(stepSixArray[0] + " is ")
			} else if(stepSixArray.length === 2) {
				$(".stepText").show();
				$("#names").html("<span class='friendName'>" + stepSixArray[0] + "</span>" + " and " + "<span class='friendName'>" + stepSixArray[1] + "</span>" + " are ");
			} else {
				$(".stepText").show();
				$("#names").html("<span class='friendName'>" + stepSixArray[0] + "</span>" + ", " + "<span class='friendName'>" + stepSixArray[1] + "</span>" + " and " + (stepSixArray.length - 2) + " others are ");
			};
		});
}); //getJSON

}); //document.ready